<?php

    require_once('../config.php');

    // ! ADICIONAR UM NOVO ADM NO BANCO 

    if(isset($_POST['cadastro']))
    {
        $adm = new Administrador
        (
            $_POST['nome'],
            $_POST['email'],
            $_POST['login'],
            $_POST['senha']
        );        

        if(isset($_POST['senha']) == isset($_POST['confirma_senha']))
        {
            $adm->insert();
            header("Location:principal.php?link=11&msg=ok");
        }   
    }
    
    // ! EXCLUIR OS ADM'S NO BANCO

    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir'); 

    if(isset($id)&& $excluir==1)
    {
        $admin = new Administrador();
        $admin->setId($id);
        $admin->delete();
        header("Location:principal.php?link=11&msg=ok");
    }

    // ! LISTAR E ATUALIZAR OS ADM'S NO BANCO

    if(isset($_POST['alterar_adm']))
    {
        $adm = new Administrador
        (
            $_POST['alterar_nome'],
            $_POST['alterar_email'],
            $_POST['alterar_login']            
        );        
        
        if($adm->getId() >0 )
        {
            header("Location:principal.php?link=11&msg=usuario alterado com sucesso");
        }
    }

    // ! LOGIN DO ADMINISTRADOR 

    $adm_login = new Administrador();

    if(isset($_POST['logar_adm']) && isset($_POST['login_adm']))
    {
        $adm_login->efetuarlogin
        (
            $_POST['login_adm'],
            $_POST['senha_adm']
        );

        // TODO: FAZENDO O LOGIN E ABRINDO SESSÃO 

        if($adm_login->getId() >0 )
        {
            $_SESSION['logado'] = true;
            $_SESSION['id_adm'] = $adm_login->getId();
            $_SESSION['nome_adm'] = $adm_login->getNome();
            $_SESSION['login_adm'] = $adm_login->getLogin();
            $_SESSION['email_adm'] = $adm_login->getEmail();
            header('Location: principal.php');                
        }
    }    
    
    // TODO: FINALIZANDO A SESSÃO DO ADMINISTRADOR

    if(isset($_GET['sair']))
    {        
        if($_GET['sair'])
        {
            $_SESSION['logado'] = false;
            $_SESSION['id_adm'] = null;
            $_SESSION['nome_adm'] = null;
            $_SESSION['login_adm'] = null;
            header('Location: adm_login.php'); 
        }
    }
    
        
    

?>