<?php
    require_once('../config.php');
    if(!$_SESSION['logado'])
    {
        header('Location: adm_login.php');
    }

?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Administrador - 
        <?php
            echo $_SESSION['nome_adm'];
        ?>
    </title>

    <link rel="stylesheet" href="css/estilo.css">

    <link rel="shortcut icon" href="img/administrator_3552.ico" type="image/x-icon">

</head>
<body>
   

    <div id="principal">

        <div id="cabecalho">

            <div id="titulo_topo">
                <img src="img/AREAE_ADM.png" alt="">
                <br>
                <p>(<a href="op_administrador.php?sair=true">SAIR</a>)- <?php echo $_SESSION['nome_adm'];?></p>
            </div>

        </div> <!--FINAL CABEÇALHO -->

        <div id="corpo">
            
            <div id="esquerdo">

                <!-- SESICTION 1 -->

                <div id="sessao">Categoria

                    <ul>
                        <li><a href="principal.php?link=2">Cadastras</a></li>
                        <li><a href="principal.php?link=3">Editar</a></li>
                    </ul>

                </div>

                <!-- SESICTION 2 -->

                <div id="sessao">Post

                    <ul>
                        <li><a href="principal.php?link=4">Cadastras</a></li>
                        <li><a href="principal.php?link=5">Editar</a></li>
                    </ul>
    
                </div>

                <!-- SESICTION 3 -->

                <div id="sessao">Notícia

                    <ul>
                        <li><a href="principal.php?link=6">Cadastras</a></li>
                        <li><a href="principal.php?link=7">Editar</a></li>
                    </ul>
        
                </div>

                <!-- SESICTION 3 -->

                <div id="sessao">Banner

                    <ul>
                        <li><a href="principal.php?link=8">Cadastras</a></li>
                        <li><a href="principal.php?link=9">Editar</a></li>
                    </ul>
        
                </div>

                <!-- SESICTION 2 -->

                <div id="sessao">Administrator

                    <ul>
                        <li><a href="principal.php?link=10">Cadastras</a></li>
                        <li><a href="principal.php?link=11">Editar</a></li>
                    </ul>
    
                </div>

                <div id="sessao">Usuário

                    <ul>
                        <li><a href="principal.php?link=12">Cadastras</a></li>
                        <li><a href="principal.php?link=13">Editar</a></li>
                    </ul>
    
                </div>

            </div> <!-- FINAL DO ESQUERDO-->

            <div id="direito">

                <?php

                    if(isset($_GET['link']))
                    {
                        $link = $_GET['link'];
                        $pag[1]="principal.php";
                        
                        $pag[2]="frm_categoria.php";
                        $pag[3]="lista_categoria.php";
                        $pag[4]="frm_post.php";
                        $pag[5]="lista_post.php";
                        $pag[6]="frm_noticia.php";
                        $pag[7]="lista_noticia.php";
                        $pag[8]="frm_banner.php";
                        $pag[9]="lista_banner.php";
                        $pag[10]="frm_administrador.php";
                        $pag[11]="lista_administrador.php";
                        $pag[12]="frm_user.php";
                        $pag[13]="lista_user.php";

                        if(!empty($link))
                        {
                            if(file_exists($pag[$link]))
                            {
                                include $pag[$link];
                            }

                            else
                            {
                                include $pag[1];
                            }

                        }                        

                    }

                    else
                    {
                        include "home.php";
                    }

                ?>

            </div> <!-- FINAL DA DIV -->

        </div>

    </div>

    
</body>
</html>