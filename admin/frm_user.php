
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Cadastro Usuario</title>
</head>
<body>
    
<div id="formulario-menor">

        <form action="op_user.php" method="POST">

            <legend>NOVO USUÁRIO</legend>

            <fieldset>

                <input type="hidden" id="id" name="id">
                <label for="">Nome</label>
                <input type="text" name="nome_user" required>
                <p>

                <label for="">Email</label>
                <input type="text" name="email_user" required>
                <p>

                <label for="">Login</label>
                <input type="text" name="login_user" required>
                <p>

                <label for="">Senha</label>
                <input type="password" name="senha_user" required>
                <p>

                <label for="">Confirma Senha</label>
                <input type="password" name="confirma_senha_user" required>
                <p>

                <label for="">Foto de Perfil</label>
                <input type="file" name="foto_user" required>
                <p>

                <input type="submit" name="cadastrar_usuario" value="Adicionar" class="botao">
            </fieldset>    
        </form>

    </div>
    <br>
    <hr>
    <br>    
    <div id="usuarios">
        <legend>USUÁRIOS CADASTRADOS</legend>
        <?php

        require_once('../config.php');

        $usuarios = Usuario::getList()                         ;
        foreach ($usuarios as $usuario) 
        {
            echo "<img src='foto/".$usuario['foto']."' width='56' height='48'>";
            echo "<br>";
            echo "<b>Nome:</b> ".$usuario['nome'];
            echo "<br>";
            echo "<b>Email:</b> ".$usuario['email'];
            echo "<br>";
        }
    ?>       
    </div>
</body>
</html>