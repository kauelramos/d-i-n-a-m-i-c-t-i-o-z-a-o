create database dinamico85db;

use dinamico85db;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Estrutura da tabela `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `administrador`
--

INSERT INTO `administrador` (`id`, `nome`, `email`, `login`, `senha`) VALUES
(1, 'Wellington Vieira', 'wellington@dev.senac', 'wellington', '123456');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(11) NOT NULL,
  `titulo_banner` varchar(255) NOT NULL,
  `link_banner` varchar(255) NOT NULL,
  `img_banner` varchar(150) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `banner_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `cat_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id_noticia` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_noticia` varchar(255) NOT NULL,
  `img_noticia` varchar(100) NOT NULL,
  `visita_noticia` int(11) NOT NULL,
  `data_noticia` date NOT NULL,
  `noticia_ativo` varchar(1) NOT NULL,
  `noticia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `titulo_post` varchar(250) NOT NULL,
  `descricao_post` text NOT NULL,
  `img_post` varchar(200) NOT NULL,
  `visitas` int(11) NOT NULL,
  `data_post` date NOT NULL,
  `post_ativo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id_noticia`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id_noticia` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

-- 
-- TABELA USUARIOS
-- 

CREATE TABLE `usuario`
(
  id int(11) NOT NULL PRIMARY KEY auto_increment,
  nome VARCHAR(250) NOT NULL,
  email VARCHAR(10) NOT NULL,  
  login varchar(100) NOT NULL,
  senha varchar(50) NOT NULL,
  foto VARCHAR(100)
);


SELECT * FROM usuario;



-- ----------------------------
-- --- CRIANDO STORE PROCEDURES
-- ----------------------------

DELIMITER $$

CREATE PROCEDURE `SP_Insert_Noticia`
(
	_id_noticia INT(11),
    _id_categoria INT(11),
    _titulo_noticia VARCHAR(255),    
	_img_noticia varchar(100),
	_visita_noticia int(11),
	_data_noticia date,
	_noticia_ativo varchar(1),
	_noticia text
)
BEGIN
	INSERT INTO noticia(id_categoria, titulo_noticia, img_titulo, visitas_noticias, data_noticia, visitas_noticia, noticia)
    values(_id_categoria, _titulo_noticia, _img_titulo, _visitas_noticias, _data_noticia, _visitas_noticia, _noticia);
    SELECT * FROM noticia WHERE id = (select @@identity);
END $$

DELIMITER $$

CREATE PROCEDURE `Sp_Insert_Post`
(
	_id_categoria INT(11),
    _titulo_post VARCHAR(250),
    _descricao_post TEXT,
    _img_post VARCHAR(200),
    _visitas VARCHAR(11),
    _data_post DATE,
    _post_ativo VARCHAR(1)
)
BEGIN
	INSERT INTO post(id_categoria, titulo_post, decricao_post, img_post, visitas, data_post, post_ativo)
    VALUES(_id_categoria, _titulo_post, _decricao_post, _img_post, _visitas, _data_post, _post_ativo);
	SELECT * FROM post WHERE id = (select @@IDENTITY);
END$$

DELIMITER $$

CREATE PROCEDURE `Sp_Insert_Banner`
(
	_titulo_banner VARCHAR(255),
    _link_banner VARCHAR(255),
    _img_banner VARCHAR(150),
    _alt VARCHAR(255),
    _banner_ativo VARCHAR(1)
)
BEGIN
	INSERT INTO banner(titulo_banner, link_banner, img_banner, alt,banner_ativo)
    VALUES(_titulo_banner, _link_banner, _img_banner, _alt, _banner_ativo);
    SELECT * FROM banner WHERE id = (select @@identity);
END$$

DELIMITER $$
CREATE PROCEDURE `SP_Insert_Usuario`(
_nome varchar(200),
_email varchar(200),
_login varchar(100),
_senha varchar(50),
_foto varchar(100)
)
BEGIN
	insert into usuario (nome, emaIl, login, senha, foto)
    values (_nome, _email, _login, _senha, _foto);
    select * from usuario where id = (select @@identity);
END$$

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Insert_Administrador`(
_nome varchar(200),
_email varchar(200),
_login varchar(100),
_senha varchar(50)
)
BEGIN
	insert into administrador (nome, emaIl, login, senha)
    values (_nome, _email, _login, _senha);
    select * from administrador where id = (select @@identity);
END $$

DELIMITER $$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Insert_Categoria`(
	_categoria varchar(150),
    _cat_ativo varchar(1)
)
BEGIN
	INSERT INTO categoria (categoria, cat_ativo)
    VALUES(_categoria, _cat_ativo);
    SELECT * FROM categoria WHERE id = (select @@identity);

END $$


