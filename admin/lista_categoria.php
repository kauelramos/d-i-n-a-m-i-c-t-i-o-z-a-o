<!DOCTYPE html>
<html lang="en">
<head>       
    <title>Lista categoria</title>

    <link rel="stylesheet" href="css/estilo.css">

</head>
<body>

    <table id="tb_categoria" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#069">
        <tr bgcolor="#2e3192" align="center">
            <th width="5%" height="2" ><font size="2" color="#fff">Codigo</font></th>
            <th width="25%" height="2" ><font size="2" color="#fff">Categoria</font></th>
            <th width="15%" height="2" ><font size="2" color="#fff">Ativo</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>

        <?php
            require_once('../config.php');
            $categorias = Categoria::ListaCat();
            foreach($categorias as $categoria)
            {
                    
        ?> 


        <tr>
            <td>
                <font size="2" face="verdana, arial" color="#fff">
                <?php echo $categoria['id_categoria']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $categoria['categoria']; ?>
                </font>
            </td>
            <td>
                <font size="2" face="verdana, arial" color="#fff">
                    <?php echo $categoria['cat_ativo']=='1'?'Sim':'Não'; ?>
                </font>
            </td>
            <td align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo "frm_alterar_categoria.php?id" 
                        .$categoria['id']
                        ."$categoria=".$categoria['categoria']
                        ."$cat_ativo=".$categoria['cat_ativo']
                    ?> ">Alterar</a>
                </font>
            </td>

            <td align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo"op_categoria.php?excluir=1&id_categoria=".$categoria['id']; ?>">Excluir</a>
                </font>
            </td>
        </tr>

        <?php } ?>

    </table>    
    
</body>
</html>

