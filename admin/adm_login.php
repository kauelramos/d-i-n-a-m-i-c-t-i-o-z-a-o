<?php

# IMPORTA OS ARQUVIOS DE CONFIG PADRÃO

require_once('../config.php');
if(isset($_SESSION['logado']))
{

    if($_SESSION['logado'])
    {
        header('Location: principal.php');
    }
    
}

?>

<!DOCTYPE html>
<html lang="pt-Br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Site Adesivo - Area do Adm</title>

    <link rel="stylesheet" href="css/estilo.css">
    
</head>
<body>

    <!--  -->

    <div id="box-login">

        <div id="sair_login_adm">
            <a href="../index.php">VOLTAR</a>
        </div>

        <div id="formulario-login">

            <form id="frmlogin" name="frmlogin" action="op_administrador.php" method="post">            

                <fieldset>

                    <legend>Área Administrativa</legend>

                    <label for=""><span>login</span></label>
                    <input type="text" name="login_adm" id="txt_login">

                    <label for=""><span>Senha</span></label>
                    <input type="password" name="senha_adm" id="txt_senha">

                    <input type="submit" name="logar_adm" id="logar" value="logar" class="botao">
                    <br>
                    <span><?php echo(isset($_GET['msg']))?$_GET['msg']:"" ?></span>  
                    
                </fieldset>

            </form>

        </div>

    </div>    
    
</body>
</html>