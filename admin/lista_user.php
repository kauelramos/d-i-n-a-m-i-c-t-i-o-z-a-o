<!DOCTYPE html>
<html lang="en">
<head>       
    <title>Lista Administrador</title>

    <link rel="stylesheet" href="css/estilo.css">

</head>
<body>

    <table id="tb_adm" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#069">
        <tr bgcolor="#2e3192" align="center">
            <th width="15%" height="2" ><font size="2" color="#fff">Id</font></th>
            <th width="15%" height="2" ><font size="2" color="#fff">Nome</font></th>
            <th width="15%" height="2" ><font size="2" color="#fff">Email</font></th>
            <th width="15%" height="2" ><font size="2" color="#fff">Login</font></th>
            <th colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>

        <?php
            require_once('../config.php');
            $users = Usuario::getList();
            foreach($users as $user)
            {
                    
        ?> 

        <tr>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $user['id']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $user['nome']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $user['email']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $user['login']?></font></td>
            <td align="center">
                <font size="2" face="verdana, arial" color="#ffff">
                    <a href="<?php echo "frm_alterar_administrador.php?id"
                    .$user['id']
                    ."&nome=".$user['nome']
                    ."&email=".$user['email']
                    ."&login=".$user['login'];
                    ?>">Alterar</a>
                </font>
            </td>

            <td></td>

        </tr>
        
        <?php } ?>

    </table>    
    
</body>
</html>