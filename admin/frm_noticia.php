
<div id="formulario-menor">

        <form action="op_noticia.php" method="POST">

            <legend>NOVA NOTICIA</legend>

            <fieldset>

                <input type="hidden" id="id" name="id">
                <label for="">Categoria</label>

                <!-- // * SELECIONAMDO A CATEGORIA DA NOTICIA  -->

                <?php
                    require_once('../config.php');
                    $categorias = Categoria::ListaCat();
                ?>
                <select name="idcategoria" id="idcategoria">
                    <?php
                        foreach($categorias as $cat)
                        {                        
                    ?>
                    <option value="<?php echo $cat['id_categoria'].'">'.$cat['id_categoria']."-".$cat['categoria']; ?>"></option>
                    <?php } ?>
                </select>
                <p>

                <label for="">Titulo Noticia</label>
                <input type="text" name="txt_noticia" required>
                <p>

                <label for="">Imagem Noticia</label>
                <input type="file" name="img_noticia" required>
                <p>

                <label for="">Data</label>
                <input type="date" name="data" required>
                <p>

                <label for="">Texto Noticia</label>
                <textarea name="texto_noticia" id="txtnoticia" cols="30" rows="10"></textarea>
                <p>

                <div>
                    <p id="p_ativo">Ativo <input type="checkbox" name="status_not" id="check_ativo" checked> </p>
                </div>
                <p>
                <input type="submit" name="cadastrar_not" value="Adicionar" class="botao">
            </fieldset>    
        </form>

</div>
