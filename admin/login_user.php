<?php
    require_once('../config.php');
    if(isset($_SESSION['logado_user']))
    {

        if($_SESSION['logado_user'])
        {
            header('Location: ../index.php');
        }
    
    }        
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Login Usuario</title>

    <link rel="stylesheet" href="css/estilo.css">
</head>
<body>
    <div id="box-login">
        
        <div class="voltar_home">
            <a href="../index.php">VOLTAR</a>
        </div>

        <div id="formulario-login">
        
            <form id="frmlogin" name="frmlogin" action="op_user.php" method="POST">
        
                <fieldset>
        
                    <legend>Login usuario</legend>
        
                    <label for=""><span>login</span></label>
                    <input type="text" name="txt_login_user" id="txt_login_user">
        
                    <label for=""><span>Senha</span></label>
                    <input type="password" name="txt_senha_user" id="txt_senha_user">
        
                    <input type="submit" name="login" id="logar" value="LOGIN" class="botao">
                    <br>
                    <span>
                        <?php echo(isset($_GET['msg']))?$_GET['msg']:"" ?>                        
                    </span>
                    
                </fieldset>
        
            </form>
        
        </div>
        
    </div>    
    
</body>
</html>