<?php 
    require_once('../config.php'); 
    

    // ! ADICIONAR NOVO USUARIO NO BANCO

    if(isset($_POST['cadastrar_usuario']))
    {      
        $img = imagem_user();
        $new_user = new Usuario
        (
            $_POST['nome_user'],
            $_POST['email_user'],
            $_POST['login_user'],
            $_POST['senha_user'],
            $img[0]
        );
        
        if(isset($_POST['senha_user']) == isset($_POST['confirma_senha_user']))
        {
            $new_user->insert();            
        }
        
        if($new_user->getId()!=null)
        {
            header("location:principal.php?link=13&msg=ok");
        }
    }

    // ! EXCLUI O USUARIO DO BANCO DE DADOS

    $id = filter_input(INPUT_GET,'id');
    $excluir = filter_input(INPUT_GET,'excluir');

    if(isset($id) && $excluir==1)
    {
        $user = new Usuario();
        $user->setId($id);
        $user->delete();
        header("Location:principal.php?link=13&msg=ok");
    }

    // ! EFETUAR LOGIN DO USUARIO    
    
    if(isset($_POST['login']) && isset($_POST['txt_login_user']))
    {   

        // $_login_user = isset($_POST['txt_login_user'])?$_POST['txt_login_user']:"";
        // $_senha_user = isset($_POST['txt_login_senha'])?$_POST['txt_login_senha']:"";


        $user_login = new Usuario();
        $user_login->efeturarLogin($_POST['txt_login_user'], $_POST['txt_senha_user']);
        
        // ! CASO O LOGIN SEJA REALIZADO

        if($user_login->getId() >0 )
        {
            // TODO: SESSÃO ABERTO DO USUARIO
            $_SESSION['logado_user'] = true;
            $_SESSION['id_user'] = $user_login->getId();
            $_SESSION['nome_user'] = $user_login->getNome();
            $_SESSION['login_user'] = $user_login->getLogin();
            $_SESSION['email_user'] = $user_login->getEmail();            
            header('Location: ../index.php');
        }

        // TODO: FINALIZANDO A SESSÃO DO USUARIO

        if(isset($_GET['LOGOUT']))
        {           
            if($_GET['LOGOUT'])
            {
                $_SESSION['logado_user'] = false;
                $_SESSION['id_user'] = null;
                $_SESSION['nome_user'] = null;
                $_SESSION['login_user'] = null;
                $_SESSION['email_user'] = null;
                header('Location: login_user.php'); 
            }
        }


        if(isset($_POST['txt_login_user'])!==NULL & isset($_POST['txt_senha_user'])!==NULL)
        {   
            //  ! CASO O USUARIO E SENHA ESTEJAM INCORRETOS

            if($user_login->getId()==NULL)
            {
                header('Location: login_user.php?msg=Login e Senha incorretos');
            }            
        }

        // ! CASO O USUARIO E SENHA ESTEJAM EM BRANCO

        if(empty($_POST['txt_login_user']) && empty($_POST['txt_senha_user']))
        {
            header('Location: login_user.php?msg=Preencha todos os campos');                
        }
    }

    

?>