<!DOCTYPE html>
<html lang="en">
<head>       
    <title>Lista Administrador</title>

    <link rel="stylesheet" href="css/estilo.css">

</head>
<body>

    <table id="tb_adm" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#069">
        <tr bgcolor="#2e3192" align="center">
            <th width="5%" height="2"><font size="2" color="#fff">ID</font></th>
            <th width="45%" height="2"><font size="2" color="#fff">Nome</font></th>
            <th width="18%" height="2"><font size="2" color="#fff">Email</font></th>
            <th width="15%" height="2"><font size="2" color="#fff">login</font></th>
            <th width="10%"colspan="2"><font size="2" color="#fff">Opções</font></th>
        </tr>

        <?php
            require_once('../config.php');
            $adms = Administrador::getList();
            foreach($adms as $adm)
            {                    
        ?> 

        <tr>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $adm['id']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $adm['nome']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $adm['email']?></font></td>
            <td><font size="2" face="verdana, arial" color="#fff"><?php echo $adm['login']?></font></td>
            <td align="center">
                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo "frm_alterar_administrador.php?id"
                    .$adm['id']
                    ."&nome=".$adm['nome']
                    ."&email=".$adm['email']
                    ."&login=".$adm['login'];
                    ?>">Alterar</a>                    
                </font>

                <font size="2" face="verdana, arial" color="#fff">
                    <a href="<?php echo"op_administrador.php?excluir=1&id=".$adm['id']; ?>">Excluir</a>
                </font>
            </td>

            <td></td>

        </tr>

        <?php } ?>            

    </table>    
    
</body>
</html>

