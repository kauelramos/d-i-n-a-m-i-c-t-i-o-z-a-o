<?php

    $id = filter_input(INPUT_GET,'id');
    $nome = filter_input(INPUT_GET,'nome');
    $email = filter_input(INPUT_GET,'email');
    $login = filter_input(INPUT_GET,'login');
    
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Atereção de Administrador</title>

</head>
<body>    
    
    <form action="op_administrador.php" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Alteração de administrador</legend>
            <div>
                <input type="hidden" name="id" value="<?php echo $id ?>">
            </div>

            <div>
                <label for="">Nome</label>
                <input type="text" name="alterar_nome" value="<?php echo $nome ?>">
            </div>

            <div>
                <label for="">Email</label>
                <input type="text" name="alterar_email" value="<?php echo $email ?>">
            </div>

            <div>
                <label for="">Login</label>
                <input type="text" name="alterar_login" value="<?php echo $login ?>">
            </div>

            <div>
                <input type="submit" name="alterar_adm" value="Registrar">
            </div>
        </fieldset>
    </form>

</body>
</html>