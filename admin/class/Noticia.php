<?php

    class Noticia
    {

        # ATRIBUTOS DA CLASSE NOTICIA

        private $id;
        private $id_categoria;
        private $titulo;
        private $img;
        private $visitas;
        private $data;
        private $noticia_ativo;
        private $txt_not;
        

        public function setId($value)
        {
            $this->id = $value;
        }

        public function getId()
        {
            return $this->id;
        }


        public function setTitulo($value)
        {
            $this->titulo = $value;
        }

        public function getTitulo()
        {
            return $this->titulo;
        }


        public function setimagem($value)
        {
            $this->img = $value;
        }

        public function getImagem()
        {
            return $this->img;
        }


        public function setVisitas($value)
        {
            $this->visitas = $value;
        }

        public function getVisitas()
        {
            return $this->visitas;
        }


        public function setDate($value)
        {
            $this->data = $value;
        }

        public function getDate()
        {
            return $this->date;
        }


        public function setNoticia_ativo($value)
        {
            $this->noticia_ativo = $value;
        }

        public function getNoticia_ativo()
        {
            return $this->noticia_ativo;
        }


        public function setTxt_noticia($value)
        {
            $this->txt_not = $value;
        }

        public function getTxt_noticia()
        {
            return $this->txt_not;
        }



        public function loadById($_id)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM noticia WHERE id=:id ", array(':id'=>$_id));

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        public function getList()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM noticia ORDER BY titulo_noticia");

        }

        public function search($titulo_not)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM noticia WHERE titulo_noticia LIKE :titulo_noticia", array(":titulo_noticia"=>"%".$titulo_not."%"));
        }

        public function setData($data)
        {
            $this->setId($data['id_noticia']);
            $this->setCategoria($data['id_categoria']);
            $this->setTitulo($data['titulo_noticia']);
            $this->setimagem($data['img_noticia']);
            $this->setVisitas($data['visita_noticia']);
            $this->setDate($data['data_noticia']);
            $this->setNoticia_ativo($data['noticia_ativo']);
            $this->setTxt_noticia($data['noticia']);
        }

        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL Sp_Insert_Noticia(:id_categoria, :titulo_noticia, :img_noticia, :visitas_noticia, :data_noticia, :noticia_ativo, :noticia)",
                array
                (
                    "id_categoria"=>$this->getCategoria(),                    
                    "titulo_noticia"=>$this->getTitulo(),
                    "img_noticia"=>$this->getImagem(),
                    "visita_noticia"=>$this->getVisitas(),
                    "data_noticia"=>$this->getDate(),
                    "noticia_ativo"=>$this->getNoticia_ativo(),
                    "noticia"=>$this->getTxt_noticia()
                )            
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }                
        }

        public function update($_id, $_id_categoria, $_titulo_noticia, $_img_noticia, $_visitas_noticia, $_data_noticia, $_noticia_ativo, $_noticia)
        {
            $sql = new Sql();
            $sql->query("UPDATE noticia SET id_categoria = :categoria,
            titulo_noticia = :titulo,
            img_noticia = :imagem,
            visita_noticia = :visitas,
            data_noticia = :data,
            noticia_ativo = :ativo,
            noticia = :noticia WHERE id_noticia = :id",
            array
            (
                ":categoria"=>$_id_categoria,
                ":titulo"=>$_titulo_noticia,
                ":imagem"=>$_img_noticia,
                ":visitas"=>$_visitas_noticia,
                ":data"=>$_data_noticia,
                ":ativo"=>$_noticia_ativo,
                ":noticia"=>$_noticia,
                ":id"=>$_id
            )
            );
        }

        public function updateVisita($id)
        {
            $sql = new Sql();
            $sql->query("UPDATE noticia SET visita_noticia = visita_notica +1 WHERE id_notica = :id",
                array(
                    ":id"=>$id
                )
            );
        }

    }

?>