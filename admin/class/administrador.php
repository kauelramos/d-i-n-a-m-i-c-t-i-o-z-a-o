<?php
    # CRIAR A CLASS ADMINISTRADOR

    class Administrador 
    {        
        # ATRIBUTO

        private $id;
        private $nome;
        private $email;
        private $senha;
        private $login;

        # METODOS DE ASCESSO 

        # --ID--
        public function getId()
        {
            return $this->id;            
        }
        
        public function setId($value)
        {
            $this->id = $value;
        }

        # --NOME--

        public function getNome()
        {
            return $this->nome;            
        }
        public function setNome($value)
        {
            $this->nome = $value;
        }       

        # --EMAIL--

        public function getEmail()
        {
            return $this->email;            
        }

        public function setEmail($value)
        {
            $this->email = $value;
        }

        # --SENHA--

        public function getSenha()
        {
            return $this->senha;            
        }

        public function setsenha($value)
        {
            $this->senha = $value;
        }

        # --LOGIN--

        public function getLogin()
        {
            return $this->login;            
        }

        public function setLogin($value)
        {
            $this->login = $value;
        }

        # ---- SQL ---- 

        public function loadById($_id)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM administrador WHERE id=:id", array(':id'=>$_id));

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        # 

        public function getList()
        {
            $sql = new Sql();
            return $sql->select("SElECT * FROM administrador ORDER BY nome");
        }

        #

        public function search($nome_adm)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM administrador WHERE nome LIKE :nome",array(":nome"=>"%".$nome_adm."%"));
        }

        #

        public function efetuarlogin($_login, $_senha)
        {
            $sql = new Sql();            
            $senha_cript = md5($_senha);
            $results = $sql->select("SElECT * FROM administrador WHERE login = :login AND senha = :senha",
                array
                (
                    ':login'=>$_login,
                    ':senha'=>$senha_cript
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        #

        public function setData($data)
        {
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setEmail($data['email']);
            $this->setLogin($data['login']);
            $this->setSenha($data['senha']);
        }

        # INSERT

        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL SP_Insert_Administrador(:nome, :email, :login, :senha)",
                array
                (
                    "nome"=>$this->getNome(),
                    "email"=>$this->getEmail(),
                    "login"=>$this->getLogin(),
                    "senha"=>md5($this->getSenha())              
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        # UPDATE

        public function update($_id, $_nome, $_email, $_senha)
        {
            $sql = new Sql();
            $sql->query("UPDATE administrador SET nome = :nome, email = :email, login = :login WHERE id = :id",
                array
                (
                    ":id"=> $_id,
                    ":nome"=> $_nome,
                    ":email"=> $_email,                    
                    ":senha"=>md5($_senha)                
                )
            ); 
        }

        # DELETE

        public function delete()
        {
            $sql = new Sql();
            $sql->query("DELETE FROM administrador WHERE id = :id", array(":id"=>$this->getId()));
        }

        # CONSTRUCT

        public function __construct($_nome="", $_email="",$_login="",$_senha="")
        {
            $this->nome = $_nome;   
            $this->email = $_email;   
            $this->login = $_login;   
            $this->senha = $_senha;   
        }
    }

?>