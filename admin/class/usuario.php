<?php

    # CRIANDO CLASSE USUARIO

    class Usuario
    {
        private $id;
        private $nome;
        private $email;
        private $login;
        private $senha;
        private $foto;

        // METODOS DA CLASSE

        # -- ID

        public function setId($value)
        {
            $this->id = $value;
        }

        public function getId()
        {
            return $this->id;
        }

        # -- NOME

        public function setNome($value)
        {
            $this->id = $value;
        }

        public function getNome()
        {
            return $this->nome;
        }

        # --LOGIN

        private function setLogin($value)
        {
            $this->login = $value;
        }

        public function getLogin()
        {
            return $this->login;
        }

        # -- EMAIL

        public function setEmail($value)
        {
            $this->email = $value;
        }

        public function getEmail()
        {
            return $this->email;
        }

        # -- SENHA

        public function setSenha($value)
        {
            $this->senha = $value;
        }

        public function getSenha()
        {
            return $this->senha;
        }

        # -- FOTO

        public function setFoto($value)
        {
            $this->foto = $value;
        }

        public function getFoto()
        {
            return $this->foto;
        }

        # METODOS DA CLASSE

        public function loadById($_id)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM usuario WHERE id=:id", array(':id'=>$_id));

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        // * GERAR LISTA DE USUARIOS

        public function getList()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM usuario ORDER BY id");
        }

        // * PESQUISAR NOME DE USUARIOS

        public function search($nome_user)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM usuario WHERE id LIKE :id", array(":id"=>"%".$nome_user."%"));
        }

        // * EFETUAR LOGIN DO USUARIO  

        public function efeturarLogin($_login, $_senha)
        {
            $sql = new Sql();
            $cript_senha = md5($_senha);
            $results = $sql->select('SELECT * FROM usuario WHERE login = :login AND senha = :senha',
                array
                (
                    ':login'=>$_login,
                    ':senha'=>$cript_senha
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        // * ENVIAR DADOS PARA O BANCO

        public function setData($data)
        {
            $this->setId($data['id']);
            $this->setNome($data['nome']);
            $this->setLogin($data['login']);
            $this->setSenha($data['senha']);
            $this->setEmail($data['email']);
            $this->setFoto($data['foto']);
        }

        // * INSERI UM NOVO USUARIO NO BANCO

        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL SP_Insert_Usuario(:nome, :email, :login, :senha, :foto)",
                array
                (
                    "nome"=>$this->getNome(),
                    "email"=>$this->getEmail(),
                    "login"=>$this->getLogin(),
                    "senha"=>md5($this->getSenha()),
                    "foto"=>$this->getFoto()
                )
            );
            
            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        // * ATUALIZA A TABELA USUARIO

        public function update($_id, $_nome, $_email, $_login , $_senha ,$_foto)
        {
            $sql = new Sql();
            $sql->query("UPDATE usuario SET nome = :nome, email = :email, login = :login, senha = :senha ,foto = :foto WHERE id = :id",
                array
                (
                    ":id"=>$_id,
                    ":nome"=>$_nome,
                    ":emial"=>$_email,
                    ":login"=>$_login,
                    ":senha"=>$_senha,
                    ":foto"=>$_foto
                )
            );
        }

        // * DELETA USUARIO DA TABELA

        public function delete()
        {
            $sql = new Sql();
            $sql->query("DELETE FROM usuario WHERE id = :id", array(":id"=>$this-getId()));
        }

        // * CONSTRUTOR DA CLASSE

        public function __construct($_nome="",$_email="",$_login="" , $_senha="" ,$_foto="")
        {
            $this->nome = $_nome;
            $this->email = $_email;
            $this->login = $_login;
            $this->senha = $_senha;
            $this->foto = $_foto;
        }

    }

?>