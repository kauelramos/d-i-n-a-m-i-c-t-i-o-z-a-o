<?php
    # CRIANDO CALSSE PARA CATEGORIA

    class Categoria
    {
        # ATRIBUTOS DA CLASSE

        private $id_cat;
        private $nome_cat;
        private $status_cat;       

        # --ID--
        public function getId()
        {
            return $this->id_cat;
        }

        public function setId($value)
        {
            $this->id_cat = $value;
        }

        # --DESCRIÇÃO--

        public function getNome_cat()
        {
            return $this->nome_cat;
        }

        public function setNome_cat($value)
        {
            $this->Nome_cat = $value;
        }

        # --STATUS--

        public function getStatus()
        {
            return $this->status_cat;
        }

        public function setStatus($value)
        {
            $this->status_cat = $value;
        }

        # ---- METODOS DA CLASSE ----

        public function loadByid($_id)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM categoria WHERE id=:id", array(':id'=>$_id));

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        # --- GERAR UM LISTA POR ORDER DE NOME ---

        public function ListaCat()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM categoria ORDER BY categoria");            
        }

        # --- PESQUISA NOMES DE CATEGORIAS NO BANCO DE DADOS ---

        public static function search($nome_cat)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM categoria WHERE categoria LIKE :categoria", array(":categoria"=>"%".$nome_cat."%"));
        }

        # --- ELE ENVIA OS DADOS PARA O BANCO---

        public function setData($data)
        {
            $this->setId($data['id_categoria']);
            $this->setNome_cat($data['categoria']);
            $this->setStatus($data['cat_ativo']);
        }

        # --- INSERI UMA CATEGORIA NOVA NO BANCO ---

        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL SP_Insert_Categoria(:categoria, :cat_ativo)",
                array
                (
                    ":categoria"=>$this->getNome_cat(),
                    ":cat_ativo"=>$this->getStatus()
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        # --- ATUALIZA A TABELA CATEGORIAS --- 

        public function update($_id, $_categoria, $_cat_ativo)
        {
            $sql = new Sql();
            $sql->query("UPDATE categoria SET categoria = :categoria, cat_ativo = :cat_ativo WHERE id = :id",
                array
                (
                    ":id"=> $_id,
                    ":categoria"=> $_categoria,
                    ":cat_ativo"=> $_cat_ativo
                )
            );
        }

        # --- DELETA O ID INDICADO DA TABELA CATEGORIA --- 

        public function delete()
        {
            $sql = new Sql();
            $sql->query("DELETE FROM categoria WHERE id = :id", array(":id"=>$this->getId()));
        }

        # --- CONSTRUTORA ---

        public function __construct($_categoria="", $_cat_ativo="")
        {
            $this->categoria = $_categoria;
            $this->status = $_cat_ativo;  
        }       

    }
?>