<?php

    class Post
    {

        # ATRIBUTOS DA CLASSE

        private $id;
        private $titulo;
        private $descricao;
        private $imagem;
        private $visitas;
        private $date;
        private $status;

        // GET E SET - ID

        public function setId($value)
        {
            $this->id = $value;
        }

        public function getId()
        {
            return $this->id;
        }

        // GET E SET - TITULO

        public function setTitulo($value)
        {
            $this->id = $value;
        }

        public function getTitulo()
        {
            return $this->id;
        }

        // GET E SET - DESCRIÇÃO

        public function setDescricao($value)
        {
            $this->descricao = $value;
        }

        public function getDescricao()
        {
            return $this->descricao;
        }

        // GET E SET - IMAGEM

        public function setImg($value)
        {
            $this ->img = $value;
        }

        public function getImg()
        {
            return $this->img;
        }

        // GET E SET - VISITAS

        public function setVisitas($value)
        {
            $this->visitas = $value;
        }

        public function getVisitas()
        {
            return $this->visitas;
        }

        // GET E SET - DATA

        public function setDate($value)
        {
            $this->data = $value;
        }

        public function getDate()
        {
            return $this->data;
        }

        // GET E SET - STATUS

        public function setStatus($value)
        {
            $this->status = $value;
        }

        public function getStatus()
        {
            return $this->status;
        }

        # METODOS DA CLASSE

        // ASSOCIAR O ID DA CATEGORIA NO POST

        public function idCategoria($id_categoria)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM categoria WHERE id=:id", array(':id=>$id_categoria'));
            
            if(count($results[0]))
            {
                $this->setData($results[0]);
            }

        }

        // CARREGAR ID DE POSTS

        public function loadByid($id_post)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM post WHERE id=:id", array(':id'=>$id_post));

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        // GERAR UMA LISTA 

        public function getList()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM post ORDER BY titulo_post");
        }

        // PROCURAR POR NOMES DE POSTS

        public function search($titulo_post)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM post WHERE titulo_post LIKE :titulo_post", array("titulo_banner"=>"%".$titulo_post."%"));
        }

        // SET DATA

        public function setData($data)
        {
            $this->setId($data['id_post']);
            $this->setDescricao($data['descricao_post']);
            $this->setTitulo($data['titulo_post']);
            $this->setImg($data['img_post']);
            $this->setVisitas($data['visitas']);
            $this->setDate($data['data_post']);
            $this->setStatus($data['post_ativo']);
        }

        public function insert()
        {
            $sql = new Sql();
            $results = $sql->select("CALL Sp_Insert_Post(:tituli_post, :descricao_post, :img_post, :visitas, :data_post, :post_ativo)", 
                array
                (
                    "titulo_post"=>$this->getTitulo(),
                    "descricao_post"=>$this->getDescricao(),
                    "img_post"=>$this->getImg(),
                    "visitas"=>$this->getVisitas(),
                    "date_post"=>$this->getDate(),
                    "post_ativo"=>$this->getStatus()
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }           
        }

        // ATUALIZAR OS POST

        public function update($_titulo_post, $_descricao_post, $_img_post, $_visitas, $_data_post, $_post_ativo)
        {
            $sql = new Sql();
            $sql->query("UPDATE post SET titulo = :titulo, descricao = :descricao");
        }

        // DELETAR POST

        public function delete()
        {
            $sql = new Sql();
            $sql->query("DELETE FROM post WHERE id = :id", array("id"=>$this->getId()));
        }

        // CONSTRUTORA

        public function __construct($_titulo_post="",$_descricao_post, $_img_post, $_visitas, $_data_post, $_post_ativo)
        {
            $this->titulo= $_titulo_post;
            $this->descricao= $_descricao_post;
            $this->imagem= $_img_post;
            $this->visitas= $_visitas;
            $this->data= $_data_post;
            $this->status= $_post_ativo;
        }

    }

?>