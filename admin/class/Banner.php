<?php

    # CRIAR CLASSE BANNER

    class Banner
    {
        # ATRIBUTOS

        private $id;
        private $titulo;
        private $link;
        private $img;
        private $alt;
        private $status;

        # ID

        public function getId()
        {
            return $this->id;
        }

        public function setId($value)
        {   
            $this->id = $value;
        }

        # TITULO

        public function getTitulo()
        {
            return $this->titulo;
        }

        public function setTitulo($value)
        {   
            $this->titulo = $value;
        }

        # LINK

        public function getLink()
        {
            return $this->link;
        }

        public function setLink($value)
        {   
            $this->link = $value;
        }

        # IMG

        public function getImg()
        {
            return $this->img;
        }

        public function setImg($value)
        {   
            $this->img = $value;
        }

        # ALT

        public function getAlt()
        {
            return $this->alt;
        }

        public function setAlt($value)
        {   
            $this->alt= $value;
        }

        # STATUS

        public function getStatus()
        {
            return $this->id;
        }

        public function setStatus($value)
        {   
            $this->id = $value;
        }

        # --- METODOS DA CLASSE --- 

        public function loadByid($_id_banner)
        {
            $sql = new Sql();
            $results = $sql->select("SELECT * FROM banner WHERE id=:id, array(':id'=>$_id_banner)");

            if(count($results))
            {
                $this->setData($results[0]);
            }
        }

        # --- GERAR UMA LISTA DE BANNERS ---

        public function getList()
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM banner ORDER BY titulo_banner");
        }

        # --- PROCURAS OS NOMES DOS BANNER CADASTRADOS ---

        public function search($titulo_banner)
        {
            $sql = new Sql();
            return $sql->select("SELECT * FROM banner WHERE titulo_banner LIKE :titulo_banner", array("titulo_banner"=>"%".$titulo_banner."%"));
        }

        # --- ELE ENVIA OS DADOS PARA O BANCO ---

        public function setData($data)
        {
            $this->setId($data['id_banner']);
            $this->setTitulo($data['titulo_banner']);
            $this->setLink($data['link_banner']);
            $this->setImg($data['img_banner']);
            $this->setAlt($data['alt']);
            $this->setStatus($data['banner_ativo']);
        }

        # --- INSERIR NOVO BANNER NO BANCO ---

        public function insert()
        {
            $sql = new Sql();
            $results = $sql-select("CALL SP_Insert_Banner(:titulo_banner, :link_banner, :img_banner, :alt_banner, :status)",
                array
                (
                    "titulo_banner"=>$this->getTitulo(),
                    "link_banner"=>$this->getLink(),
                    "img_banner"=>$this->getImg(),
                    "alt"=>$this->getAlt(),
                    "banner_ativo"=>$this->getStatus(),
                )
            );

            if(count($results)>0)
            {
                $this->setData($results[0]);
            }
        }

        # --- ATUALIZA A TABELA BANNER ---

        public function update($_id, $_titulo_banner, $_link_banner, $_img_banner, $alt, $status)
        {
            $sql = new Sql();
        }
    }


?>