<?php

    # CLASS SQL

    # A CLASS SQL HERDOU OS ATRIBUTOS DE PDO
    class Sql extends PDO
    {
        private $cn;

        public function __construct()
        {
            $this->cn = new PDO("mysql:host=localhost;dbname=dinamico85db",'root','');            
        }

        # ATRIBUIR PARAMETROS PARA QUERYS SQL

        public function setParams($comando, $parametros = array())
        {
            foreach($parametros as $key =>$value)
            {
                $this->setParam($comando, $key, $value);
            }
        }

        public function setParam($cmd, $key, $value)
        {
            $cmd->bindParam($key, $value);
        }

        public function query($comandoSql, $params = array())
        {
            $cmd = $this->cn->prepare($comandoSql);
            $this->setParams($cmd, $params);
            $cmd->execute();
            return $cmd;
        }

        public function select($comandoSql, $params = array())
        {
            $cmd = $this->query($comandoSql,$params);
            return $cmd->fetchAll(PDO::FETCH_ASSOC);
        }



        # = ATRIBUIÇÃO;
        # == COMPARÇÃO;
        # === COMPARAÇÃO ABSOLUTA(COMPARA TIPO E VALOR);
        # => ASSOCIAÇÃO;
        # -> PROPRIEDADE;
    }

?>