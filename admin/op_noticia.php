<?php

    require_once('../config.php');


    $not = new Noticia();
    if(isset($_POST['cadastrar_not']))
    {
        $imagem = upload_imagem($_FILES['img']);
        $not->insert
        (
            $_POST['idcategoria'],
            $_POST['txt_noticia'],
            $imagem[0],
            $_POST['data'],
            $_POST['texto_noticia'],
            isset($_POST['status_not'])?'s':'n'
        );

        if($not->getId() >0 )
        {
            header("location:principal.php?link=7&msg=ok");
        }
    }

?>