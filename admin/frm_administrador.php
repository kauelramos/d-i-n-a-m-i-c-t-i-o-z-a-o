<?php

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Administrador</title>
    
    <link rel="stylesheet" href="css/estilo.css">

</head>
<body>

    <div id="formulario-menor">

        <form action="op_administrador.php" method="POST">

            <legend>NOVO ADMINISTRADOR</legend>

            <fieldset>

                <input type="hidden" id="id" name="id">
                <label for="">Nome</label>
                <input type="text" name="nome" required>
                <p>

                <label for="">Email</label>
                <input type="text" name="email" required>
                <p>

                <label for="">Login</label>
                <input type="text" name="login" required>
                <p>

                <label for="">Senha</label>
                <input type="password" name="senha" required>
                <p>

                <label for="">Confirma Senha</label>
                <input type="password" name="confirma_senha" required>
                <p>

                <input type="submit" name="cadastro" value="Adicionar" class="botao">
            </fieldset>    
        </form>

    </div>
    
</body>
</html>