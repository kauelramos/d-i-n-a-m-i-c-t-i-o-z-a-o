<?php
    require_once('config.php');
    if(!$_SESSION['logado_user'])
    {
        header('Location: admin/login_user.php');
    }

?>
 
 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">     
     <title>INFOTURBO</title>

     <!-- * LINK DO CSS PRINCIPAL -->
     <link rel="stylesheet" href="css/style.css">

     <!-- * LINK DO ICONE DO SITE -->

     <link rel="shortcut icon" href="img/icone-simbolo.ico" type="image/x-icon">

 </head>
 <body>

    <div id="estrutura">

        <div id="topo"></div>
        
        <div id="btn_login_user">
            <a href="admin/login_user.php">Fazer Login</a>
            <br>
            <p>(<a href="op_user.php?logout=true">LOGOUT</a>) - <?php echo $_SESSION['nome_user'];?></p>
        </div>
        
        <!-- * ESTRUTUR DE MENU -->

        <div id="menu">
            <ul>
                <li><a href="index.php?link=1">HOME</a></li>
                <li><a href="index.php?link=">SERVIÇOS</a></li>
                <li><a href="index.php?link=">ASSISTENCIA</a></li>
                <li><a href="index.php?link=">CONTATO</a></li>                                
            </ul>                        
        </div>

        

        <!-- * ESTRUTURA DE BANNER -->

        <div id="banner" class="banner">

        </div>

        <!-- * ESTRUTURA DO SITE  -->

        <div id="corpo">

            <div id="esquerda" class="esquerda">

                <h1>Produtos</h1>
                <li>
                    <a href="index.php?link=5">Adesivo</a>
                    <a href="index.php?link=6">Adesivo</a>
                    <a href="index.php?link=7">Adesivo</a>
                    <a href="index.php?link=8">Adesivo</a>
                    <a href="index.php?link=9">Adesivo</a>
                </li>

            </div>

            <div id="centro" class="">
                <?php
                    # !!! -_- THE LIFE START HERE -_- !!!

                    $link = $_GET['link'];
                    $pag[1] = "home.php";

                    $pag[3] = "noticia_index.php";
                    $_SESSION['idnot'] = filter_input(INPUT_GET,'idnoticia');                    

                    $pag[5] = "produto.php";
                    $pag[6] = "produto.php";
                    $pag[7] = "produto.php";
                    $pag[8] = "produto.php";
                    $pag[9] = "produto.php";

                    if(!empty($link))
                    {
                        if(file_exists($pag[$link]))
                        {
                            include($pag[$link]);
                        }

                        else
                        {
                            include($pag[1]);
                        }
                    }

                    else
                    {
                        include($pag[1]);
                    }
                ?>

                <div id="adm_login">
                    <a href="admin/adm_login.php">Login Adminstrador</a>
                </div>

                <div class="footer">
                    &copy; - INFOTURBO                    
                </div>

            </div>

            <div id="direita" class="">

                <div id="noticia">

                    <?php
                        require_once('config.php');
                        $noticias = Noticia::getList();

                        $noticias = new Noticia();               

                        foreach($noticias as $noticia)
                        {
                            if($noticia['noticia_ativo']!==0)
                            { 
                    ?>

                    <h3>
                        <img src="<?php echo $noticia['img_noticia']; ?>" alt="">
                        <div id="itens-noticias">
                            <span> <?php echo $noticia ['data_noticia']; ?> </span>
                                <a href="index.php?link=3&idnoticia=<?php echo $noticia['id_noticia']; ?>">
                                    <?php echo $noticia['titulo_noticia']; ?>
                                </a>
                        </div>
                    </h3>


                <?php } }?>  

                </div>
                
            </div>

        </div>

    </div>     

 </body>
 </html>