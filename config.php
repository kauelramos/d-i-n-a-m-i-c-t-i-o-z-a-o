<?php

// ! INICIA A SENSSÃO DO USUÁRIO

if(!isset($_SESSION))
{
    session_start();
}

// ! DEFINE O HORÁRIO DA ZONA PADRÃO GMT

date_default_timezone_set('America/Sao_Paulo');

// ! CARREGA TODAS AS CLASSES DO PROJETO

// ! SPL_AUTOLOAD_REGISTER 

spl_autoload_register(function($nome_classes)
{
    $server_str = $_SERVER['REQUEST_URI'];
    $caminho = (strpos($server_str, 'admin') !==false)?'class':'admin/class';
    $nome_arquivo = $caminho.DIRECTORY_SEPARATOR.$nome_classes.".php";
    if(file_exists($nome_arquivo))
    {
        require_once($nome_arquivo);
    }

});

// ! CRIA AS CONSTANTES DO SEU SERVIDOR DE BANCO DE DADOS

define ('IP_SERVER_DB','127.0.0.1');
define ('HOSTNAME','DELL_TIOZAO');
define ('NOME_BANCO','dinamico85db');
define ('USER_DB','root');
define ('PASS_DB','');

// ! CRIANDO UMA FUNÇÃO PARA ADICIONAR A FOTO DA NOTICIA

function upload_imagem()
{
    $foto = $_FILES['img'];
    if(!empty($foto['name']))
    {
        $largura = 640;
        $altura = 425;
        $tamanho = 300000;
        $erro = array();

        if(!preg_match("/^image\/(pjpg|jpeg|png|gif|bmp)$/",$foto['type']))
        {
            $erro[1] = "Este arquivo não é uma imagem";
        }

        $dimensoes = getimagesize($foto['tmp_name']);

        if($dimensoes[0]>$largura)
        {
            $erro[2] = "A largura (".$dimensoes[0].") da imagem não é suportada, no maximo (".$largura.") ";
        }

        if($dimensoes[0]>$altura)
        {
            $erro[3] = "A altura (".$dimensoes[1].") da imagem não é suportada, no maximo (".$altura.")";
        }

        if($foto['size']>$tamanho)
        {
            $erro[4] = "O tamanho da imagem (".$foto['size'].") não é suportada, no maximo (".$tamanho.")";
        }

        if(count($erro)==0)
        {
            # RECUPERAR A EXTENÇÃO DO ARQUIVO QUE O CLIENTE ADICIONOU
            # EXT - DE EXTENÇÃO
            # ANALISA A IMAGEM ENVIADA PELO USUARIO, E PEGA APENAS UMA EXTENÇÃO E ATRINUI UMA VARIAVEL EXT(EXTENÇÃO)
            preg_match("/\.(gid|bmp|png|jpg){1}$/i",$foto['name'],$ext);

            # REGERAR UM NOME PARA A IMAGEM DO USUARIO
            $nome_img = md5(uniqid(time())).$ext[0];
            $caminho_img = "foto/".$nome_img;
            
            # FAZER UPLOAD DA IMAGEM 
            move_uploaded_file($foto['tmp_name'],$caminho_img);

            # UNIQID GERA UM ID UNICO
        }

        $imagem_info = array();
        $imagem_info [0] = $nome_img;
        $imagem_info [1] = $erro;
        return $imagem_info;
    }
}

// ! CRIANDO FUNÇÃO PARA ADICIONAR FOTO PARA USUARIO

function imagem_user()
{    
    $foto_user = $_FILES['img_user'];
    if(!empty($foto_user['name']))
    {
        $largura = 640;
        $altura = 425;
        $tamanho = 300000;
        $erro = array();

        if(!preg_match("/^image\/(pjpg|jpeg|png|gif|bmp)$/",$foto_user['type']))
        {
            $erro[1] = "Este arquivo não é uma imagem";
        }

        $dimensoes = getimagesize($foto_user['tmp_name']);

        if($dimensoes[0]>$largura)
        {
            $erro[2] = "A largura (".$dimensoes[0].") da imagem não é suportada, no maximo (".$largura.") ";
        }

        if($dimensoes[0]>$altura)
        {
            $erro[3] = "A altura (".$dimensoes[1].") da imagem não é suportada, no maximo (".$altura.")";
        }

        if($foto_user['size']>$tamanho)
        {
            $erro[4] = "O tamanho da imagem (".$foto_user['size'].") não é suportada, no maximo (".$tamanho.")";
        }

        if(count($erro)==0)
        {
            # RECUPERAR A EXTENÇÃO DO ARQUIVO QUE O CLIENTE ADICIONOU
            # EXT - DE EXTENÇÃO
            # ANALISA A IMAGEM ENVIADA PELO USUARIO, E PEGA APENAS UMA EXTENÇÃO E ATRINUI UMA VARIAVEL EXT(EXTENÇÃO)
            preg_match("/\.(gid|bmp|png|jpg){1}$/i",$foto_user['name'],$ext);

            # REGERAR UM NOME PARA A IMAGEM DO USUARIO
            $nome_img = md5(uniqid(time())).$ext[0];
            $caminho_img = "foto_user/".$nome_img;
              
            # FAZER UPLOAD DA IMAGEM 
            move_uploaded_file($foto_user['tmp_name'],$caminho_img);
            # UNIQID GERA UM ID UNICO       
        }
        $imagem_info = array();
        $imagem_info[0] = $nome_img;
        $imagem_info[1] = $erro;
        return $imagem_info;
    }    
}


?>